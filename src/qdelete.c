#include <mqueue.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    if (mq_unlink(argv[1]) == -1) {
        printf("Unlink error!\n");
        return 1;
    }
    printf("Delete {%s} successfully!\n", argv[1]);
    return 0;
}
