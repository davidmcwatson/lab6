#include <sys/inotify.h>
#include <mqueue.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

char buffer[256];

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        printf("Usage: %s [ queue ]\n", argv[0]);
        return 1;
    }

    int flags = O_RDONLY;
    char* queue = argv[1];
    mode_t perms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
    mqd_t mqd;
    struct mq_attr attr;
    ssize_t len;
    unsigned int priority;

    mqd = mq_open(queue, flags, perms, &attr);
	if(mqd == (mqd_t) -1)
    {
        printf("Error: Could not open queue %d %s\n", errno, strerror(errno));
        return 1;
    }

    if(mq_getattr(mqd, &attr) == -1)
    {
        printf("Unable to get queue attributes %d %s\n", errno, strerror(errno));
        return 1;
    }

    printf("%ld message(s) found\n", attr.mq_curmsgs);

    for(long int i = 0; i < attr.mq_curmsgs; i++)
    {
        len = mq_receive(mqd, buffer, 256, &priority);
        if(len == -1) printf("Cannot read message %d %s\n", errno, strerror(errno));

        printf("%s", buffer);
    }
}
