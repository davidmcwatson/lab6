#include <sys/inotify.h>
#include <mqueue.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

char buffer[4096];

void watch(int notifyId, mqd_t mqd, char* sFile)
{
	ssize_t len;
	char* ptr;
	char msg[256];
	struct inotify_event* event;
	time_t time_val = time(NULL);
	char* time_msg = ctime(&time_val);

	memset(msg, 0, 256);

    while (1) {
		len = read(notifyId, buffer, 4096);

		if(len <= 0) {
			printf("Error read: {%ld}\n", len);
			break;
		}

		for (ptr = buffer; ptr < buffer + len;
			 ptr += sizeof(struct inotify_event) + event->len) {

			event = (struct inotify_event *) ptr;

			if(event->mask & IN_ACCESS)
			{
				sprintf(msg, "%s IN_ACCESS : %s", sFile, time_msg);
			}

			if(event->mask & IN_OPEN)
			{
			    sprintf(msg, "%s IN_OPEN : %s", sFile, time_msg);
			}

			if(event->mask & IN_ATTRIB)
			{
			    sprintf(msg, "%s IN_ATTRIB : %s", sFile, time_msg);
			}

			if(event->mask & IN_CLOSE_WRITE)
			{
				sprintf(msg, "%s IN_CLOSE_WRITE : %s", sFile, time_msg);
			}

			if(event->mask & IN_CLOSE_NOWRITE)
			{
				sprintf(msg, "%s IN_CLOSE_NOWRITE : %s", sFile, time_msg);
			}

			if(event->mask & IN_MODIFY)
			{
				sprintf(msg, "%s IN_MODIFY : %s", sFile, time_msg);
			}

			if(strlen(msg) > 256) printf("Message too large to send\n");
			if(msg[0] != 0) mq_send(mqd, msg, 256, 1);
		}
	}
}

int main(int argc, char* argv[])
{
    if(argc != 3)
    {
        printf("Usage: %s [ queue ]  [ file ]\n", argv[0]);
        return 1;
    }

    char* sFile = argv[2];
	int notifyId, watcherId;

    int flags = O_WRONLY;
    char* queue = argv[1];
    mode_t perms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
    mqd_t mqd;
    struct mq_attr attr;

	notifyId = inotify_init();
    if(notifyId == -1)
    {
        printf("inotify instatiation error\n");
        return 1;
    }

	watcherId = inotify_add_watch(notifyId, sFile, IN_ALL_EVENTS);
	if(watcherId == -1)
	{
		printf("Unable to watch file: %s\n", sFile);
		return 1;
	}
	printf("Watcher Created for %s\n", sFile);

	mqd = mq_open(queue, flags, perms, &attr);
	if(mqd == (mqd_t) -1)
    {
        printf("Error: Could not create queue %d %s\n", errno, strerror(errno));
        return 1;
    }
    printf("Logging events to queue: %s\n", queue);

	watch(notifyId, mqd, sFile);

	return 0;
}
