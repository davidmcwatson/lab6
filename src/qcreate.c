#include <mqueue.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

int main(int argc, char *argv[]) {
    int flags;
    char * queue;
    mode_t perms;
    mqd_t mqd;
    struct mq_attr attr;

    queue = argv[1];
    flags = O_RDWR | O_CREAT;
    perms =  S_IRUSR | S_IWUSR;
    attr.mq_flags = 0;
    attr.mq_maxmsg = 50;
    attr.mq_msgsize = 256;
    mqd = mq_open(queue, flags, perms, &attr);
    if (mqd == (mqd_t) -1) {
        printf("Error!!\n");
        printf("Queue cannot be created! -- %d %s!\n", errno, strerror(errno));
        return 1;
    }
    printf("Message Queue {%s} is ready...\n", argv[1]);
    mq_close(mqd);
    return 0;
}
