all: fwatcher qread qcreate qdelete

fwatcher: src/fwatcher.c
	gcc -Wall -g -o fwatcher src/fwatcher.c -lrt

qread: src/qread.c
	gcc -Wall -g -o qread src/qread.c -lrt

qcreate: src/qcreate.c
	gcc -Wall -o qcreate src/qcreate.c -lrt

qdelete: src/qdelete.c
	gcc -Wall -o qdelete src/qdelete.c -lrt

clean:
	rm fwatcher qread qcreate qdelete
