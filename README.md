# COMP301-17A University of Waikato #
## Lab 6: File Watcher with Interprocess Communication ##

A pair of programs where one (fwatcher) watches a designatated file and logs events to a POSIX queue and the other (qread) reads the queue to display the info.

### Usage ###

fwatcher: `./ fwatcher  [ queue ] [ file ]`

qread: `./ qread . exe [ queue ]`

Running both programs with the same [ queue ] argument will result in file events being logged to the terminal.
To avoid blocking due to a full queue, run fwatcher in the background.

### Supplied Utilities ###

Included are the programs qcreate and qdelete which both take a single argument just like qread. The two programs create and delete interprocess messaging queues.